$ git config --global --edit
hint: Waiting for your editor to close the file...
[user]
        name = Mikhail Goncharov
        email = mmgoncharov@gmail.com
[filter "lfs"]
        clean = git-lfs clean -- %f
        smudge = git-lfs smudge -- %f
        process = git-lfs filter-process
        required = true



$ echo 'Lorem ipsum dolor sit amet,
consectetuer adipiscing elit.
Aenean commodo ligula eget dolor.
Aenean massa.
Cum sociis natoque penatibus et magnis dis parturient montes,
nascetur ridiculus mus.
Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
Nulla consequat massa quis enim.
Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.' >> hello_world.txt

$ git status
$ git add hello_world.txt
$ git reset
$ echo keep working hard Misha and you will succeed >> not_bad_for_start.txt
